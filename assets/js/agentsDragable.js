function dragStart(ev) {

            ev.dataTransfer.effectAllowed='move';
            ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));
            //ev.dataTransfer.setData("Text", ev.target.getAttribute('id'));
            ev.dataTransfer.setDragImage(ev.target,0,0);

            return true;
}

function dragEnter(ev) {
            event.preventDefault();
            return true;
         }

function allowDrop(ev) {
    ev.preventDefault();
}

function drop(ev,element) {

    ev.preventDefault();

    //Things needed to Do:
    //1. Recieve Agent Name
    var agentID = ev.dataTransfer.getData("text")

    //2. Obtain Receiver Unique Business ID
    var companyID = element.getAttribute("id");
    console.log(companyID+" "+agentID);

    app.ref("Users/" + agentID + "/CompanyID").set(companyID, function(error) {
        if (error) {
            console.log(error);
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
        }
    });
    loadCompanies();
}
function drag(ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}


function dragDrop(ev) {
   var data = ev.dataTransfer.getData("Text");
   ev.target.appendChild(document.getElementById(data));
   ev.stopPropagation();
   return false;
}
