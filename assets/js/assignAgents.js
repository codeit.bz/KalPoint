var app;
var companiesRef;
var usersRef;

function Init_Firebase(){

     $.blockUI({ message: '<h4><img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" />Initializing Components...</h4>' });

    var config = {

        apiKey: "AIzaSyDZ-HANbA7JkIk3irjGKjbimH0p0u-XvdA",
        authDomain: "kalpoint-317f7.firebaseapp.com",
        databaseURL: "https://kalpoint-317f7.firebaseio.com",
        storageBucket: "kalpoint-317f7.appspot.com",
    };

    firebase.initializeApp(config);

    app = firebase.database();
    companiesRef = app.ref("Company");
    usersRef = app.ref("Users");
    $.unblockUI();
    loadCompanies();

}

function loadCompanies(){

     $.blockUI({ message: '<h4><img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" />Loading Agents And Companies...</h4>' });

    var container = document.getElementById("compTable");
    container.setAttribute("style","margin-top:20px;")
    container.innerHTML = "";

    companiesRef.on("value", function(snapshot) {

        snapshot.forEach(function(data) {
            //console.log(data.key());
            console.log(data.key);
              //if(data.val().Status == "active"){
                  //agent = data.val();
              //}


        var unID = data.key;
        //console.log(snapshot);

        var h4Title = document.createElement("H2");
        h4Title.innerHTML = data.val().Name;
        h4Title.setAttribute("style","color:#000000;text-align:center");
        container.appendChild(h4Title);

        var table = document.createElement("TABLE");
        table.className = "table table-bordered";
        table.setAttribute("style","color:#000000; text-align:center;");
        container.appendChild(table);

        var dragSectionTR = document.createElement("TR");
        dragSectionTR.setAttribute("style","color:#777;text-align:center;font-size:10px;");
        table.appendChild(dragSectionTR);

        var dragSectionTD = document.createElement("TD");
        dragSectionTD.innerHTML = "DRAG AGENTS HERE";
        dragSectionTD.colSpan = 2;
        dragSectionTD.setAttribute("ondrop","drop(event,this)");
        dragSectionTD.setAttribute("ondragover","allowDrop(event)");
        dragSectionTD.setAttribute("style","padding-top:10px;padding-bottom:10px;")
        dragSectionTD.setAttribute("id",unID);
        dragSectionTR.appendChild(dragSectionTD);

        var trHead = document.createElement("TR");
        trHead.setAttribute("style","background-color:#F2F2F2; color:#777;font-size:12px;");
        table.appendChild(trHead);

        var tdHead = document.createElement("TD");
        tdHead.innerHTML = "Agents";
        trHead.appendChild(tdHead);

        var tdHead2 = document.createElement("TD");
        tdHead2.innerHTML = "Options";
        trHead.appendChild(tdHead2);

        var body= document.createElement("TBODY");
        body.setAttribute("style","background-color:#FFFFFF;");
        table.appendChild(body);
        table.className="table table-bordered";

        var agentsRef = usersRef.orderByChild("CompanyID").equalTo(unID);
            agentsRef.on("value", function(snap) {

                snap.forEach(function(data2) {
                    if(data2.val().Role == "Agent"){


                    var tempTR = document.createElement("TR");
                    tempTR.setAttribute("style","text-align:center");
                    body.appendChild(tempTR);

                    var tempTD = document.createElement("TD");
                    tempTD.innerHTML = data2.val().FirstName +" "+ data2.val().LastName;
                    tempTD.setAttribute("style","margin-left:10px")

                    var tempTD2 = document.createElement("TD");

                    var button = document.createElement("BUTTON");
                    button.className = "btn btn-link-1 launch-modal btn-danger";
                    button.innerHTML = "Remove Agent";
                    button.value = data2.key;
                    button.setAttribute("onclick","handleDelete(this)")
                    tempTD2.appendChild(button);
                    //tempTD2.setAttribute("style","float:right");
                    tempTR.appendChild(tempTD);
                    tempTR.appendChild(tempTD2);
                        }
                });


            //document.getElementById("totAge").innerHTML=agentCnt;
        });

        //var changedPost = snapshot.val();

        //console.log("The updated post title is " + changedPost.title);
            });
    });
    companiesRef.on("child_changed", function(snap) {
        loadCompanies();
    });

    usersRef.on("child_changed", function(snap) {
        loadCompanies();
    });

    loadUnassignedAgents();
}

function loadUnassignedAgents(){

    var container = document.getElementById("agentsTable");
    container.setAttribute("style","margin-top:20px;")
    container.innerHTML = "";

    var h4Title = document.createElement("H2");
    h4Title.innerHTML = "Unassigned Agents";
    h4Title.setAttribute("style","color:#000000;text-align:center");
    container.appendChild(h4Title);

    var table = document.createElement("TABLE");
    table.className = "table table-bordered";
    table.setAttribute("style","color:#000000; text-align:center;");
    container.appendChild(table);

    var trHead = document.createElement("TR");
    trHead.setAttribute("style","background-color:#F2F2F2; color:#777");
    table.appendChild(trHead);

    var tdHead = document.createElement("TD");
    tdHead.setAttribute("style","font-size:12px;")
    tdHead.innerHTML = "Agents";
    trHead.appendChild(tdHead);

    var body= document.createElement("TBODY");
    body.setAttribute("style","background-color:#FFFFFF;");
    table.appendChild(body);
    //table.className="table table-bordered";

    var emptyAgtRef = usersRef.orderByChild("CompanyID").equalTo("");
    emptyAgtRef.on("value", function(snapshot) {
        snapshot.forEach(function(data) {
            if(data.val().Role == "Agent"){
                var tempTR = document.createElement("TR");
                tempTR.setAttribute("draggable","true");
                tempTR.setAttribute("ondragstart","drag(event)");
                tempTR.setAttribute("id",data.key);
                body.appendChild(tempTR);

                var tempTD = document.createElement("TD");
                tempTD.innerHTML = data.val().FirstName +" "+ data.val().LastName;
                tempTD.setAttribute("style","margin-left:10px")
                tempTR.appendChild(tempTD);
            }
        });
        $.unblockUI();
    });

}

function handleDelete(button){
    //alert(button.value);
    var agentID = button.value
    app.ref("Users/" + agentID + "/CompanyID").set("", function(error) {
        if (error) {
            console.log(error);
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
        }
    });

    loadCompanies();
}
