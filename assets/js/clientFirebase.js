var app;
var anonymousCaller;
var agent;
var decoded_company;
var client_name;
var message;

function Init_Firebase(comID, fullClientName, msg){
    showLoading();

    $.blockUI({ message: '<h4><img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" />Making Your Call...</h4>' });

    decoded_company = comID;
    client_name = fullClientName;
    message = msg;

    var config = {

        apiKey: "AIzaSyDZ-HANbA7JkIk3irjGKjbimH0p0u-XvdA",
        authDomain: "kalpoint-317f7.firebaseapp.com",
        databaseURL: "https://kalpoint-317f7.firebaseio.com",
        storageBucket: "kalpoint-317f7.appspot.com",
    };

    firebase.initializeApp(config);

    app = firebase.database();

    getAnonymousUser();
}

function hideLoading(){

}

function showLoading(){

}



function anonymousUserRetreived(user){
    console.log(user);
    anonymousCaller = user;
    console.log("anonymouseUserRetrieved: ");
    callAgent(decoded_company, client_name, message);
    hideLoading();
    receiveUniqueUserID(anonymousCaller.full_user_id);
}



function callAgent(busID, clientName, query){
    console.log("calling agent...");
    //call(anonymousCaller);
    getAnActiveAgent(busID, clientName, query);
    //createCallSession(anonymousCaller);
}

function getAnActiveAgent(companyId, clientName, query){
    app.ref("Users").orderByChild('CompanyID').equalTo(companyId).once('value').then(function(snapshot) {
        //var agent;
          snapshot.forEach(function(data) {
              if(data.val().Status == "active"){
                  agent = data.val();
              }
          });

        createCallSession(anonymousCaller, agent.KandyU, clientName, query);
        //alert(JSON.stringify(agent));
        //return agent;
    });
}

function createCallSession(callerInfo, busCallID, clientName, query){
    console.log(callerInfo);

    app.ref("CallSession/" + callerInfo.user_name).set({
        Active: true,
        AgentID: busCallID,
        CallerID: callerInfo.full_user_id
    }, function(error) {
        if (error) {
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
            console.log("Saving query...")
            createQuery(callerInfo, busCallID, clientName, query);
            //call(busCallID);
        }
    });
}

function createQuery(callerInfo, busCallID, fullname, query){
    app.ref("CustomerQueries/" + callerInfo.user_name).set({
        FullName: fullname,
        Situation: query,
        Solved: false,
        time:""
    }, function(error) {
        if (error) {
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
            console.log("Calling Agent...")

            call(busCallID);

        }
    });
}

function callEstablished(){
    document.getElementById("agentName").innerHTML = agent.FirstName + " " + agent.LastName;
}

function issueSolved(solved){
    app.ref("CustomerQueries/" + anonymousCaller.user_name + "/Solved").set(solved, function(error) {
        if (error) {
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
            console.log("Query Solved: " + solved);
            endCallSession();
            //$('#wasResolvedModal').modal('hide');
            //call(busCallID);

        }
    });
}

function endCallSession(){
    app.ref("CallSession/" + anonymousCaller.user_name + "/Active").set(false, function(error) {
        if (error) {
            //alert("Data could not be saved." + error);
        } else {
            //alert("Data saved successfully.");
            //console.log("Query Solved: " + solved);
            $('#wasResolvedModal').modal('hide');
            //call(busCallID);

        }
    });
}
