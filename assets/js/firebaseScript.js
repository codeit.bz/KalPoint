var app;
var usersRef;
var userEmail;

function Init_Firebase(){



    if(sessionStorage.LOGIN){
        sessionStorage.LOGIN = false;

        if (sessionStorage.EMAIL) {
            // Code for localStorage/sessionStorage.
            userEmail = sessionStorage.EMAIL;
            getUserData();
            //alert(userEmail);
        } else {
            // Sorry! No Web Storage support..
            //alert("Emtpy");
        }
    }

    var config = {

        apiKey: "AIzaSyDZ-HANbA7JkIk3irjGKjbimH0p0u-XvdA",
        authDomain: "kalpoint-317f7.firebaseapp.com",
        databaseURL: "https://kalpoint-317f7.firebaseio.com",
        storageBucket: "kalpoint-317f7.appspot.com",
    };

    firebase.initializeApp(config);

    app = firebase.database();
}



function dashboardLoad(){
    $.blockUI({ message: '<img style="float:left;font-family: sans-serif;" src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" /><h4> Initializing Componets...</h4>' });
    var config = {
        apiKey: "AIzaSyDZ-HANbA7JkIk3irjGKjbimH0p0u-XvdA",
        authDomain: "kalpoint-317f7.firebaseapp.com",
        databaseURL: "https://kalpoint-317f7.firebaseio.com",
        storageBucket: "kalpoint-317f7.appspot.com",
    };

    firebase.initializeApp(config);

    app = firebase.database();
    usersRef = app.ref('Users');

    $.unblockUI();
    loadAgentNumbers();

}

function loadAgentNumbers(){

    $.blockUI({ message: '<img style="float:left;font-family: sans-serif;" src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" /><h4> Obtaining Agents...</h4>' });

    var agentCnt = 0;
    var activeCnt = 0;
    var busyCnt = 0;
    var offlineCnt = 0;

    //OBTAIN AGENTS Reference
    var agentsRef = usersRef.orderByChild("Role").equalTo("Agent");
    agentsRef.on("child_added", function(snap) {
        agentCnt++;
        document.getElementById("totAge").innerHTML=agentCnt;
    });

    //OBTAIN Active AGENTS Reference
    var agentsActiveRef = usersRef.orderByChild("Role").equalTo("Agent");
    agentsActiveRef.on("child_added", function(snap) {
        if(snap.val().Status == "active")
            activeCnt++;
        document.getElementById("actAge").innerHTML=activeCnt;
    });

    //OBTAIN Busy AGENTS Reference
    var agentsBusyRef = usersRef.orderByChild("Role").equalTo("Agent");
    agentsBusyRef.on("child_added", function(snap) {
        if(snap.val().Status == "busy")
            busyCnt++;
        document.getElementById("busAge").innerHTML=busyCnt;
    });

    //OBTAIN Offline AGENTS Reference
    var agentsOfflineRef = usersRef.orderByChild("Role").equalTo("Agent");
    agentsBusyRef.on("child_added", function(snap) {
        if(snap.val().Status == "offline")
            offlineCnt++;
        document.getElementById("offAge").innerHTML=offlineCnt;
        $.unblockUI();
    });


    //<<<<<<-----------On Change Listeners -------->>>>>>>>

    //OBTAIN  AGENTS Reference
    usersRef.on("child_changed", function(snap) {
        loadAgentNumbers();
    });


}

function login(){
    var email = document.getElementById("u_email").value;
    var password = document.getElementById("u_pass").value;
    //alert(document.getElementById("u_email").value+": " +
    //document.getElementById("u_pass").value);

    firebase.auth().signInWithEmailAndPassword(email, password).then(function(result){

        var user = firebase.auth().currentUser;

        if (user) {
          // User is signed in.
            console.log("user found");
            //alert(user.email);
            checkUserType(user.email);
        } else {
          // No user is signed in.
            console.log("no user found");
        }

        //window.location="index.html";

        //verifyRecord();

    }).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorMessage);
        //alert("Deal Hunt Was Unable To Log Into Your Account, Please Verify That Your Credentials Are Correct And Try Again.");
        //$.unblockUI();
        // ...
    });

}

function checkUserType(email){
    app.ref("Users").orderByChild('Email').equalTo(email).once('value').then(function(snapshot) {
        //console.log(JSON.stringify(snapshot.val()));
        var agent;
          snapshot.forEach(function(data) {
              if(data.val().Role == "Agent"){
                  sessionStorage.EMAIL = email;
                  //sessionStorage.LOGIN = true;
                  window.location="pages/agent.html";
              }else{
                  sessionStorage.EMAIL = email;
                  //sessionStorage.LOGIN = true;
                  window.location="index.html";
              }
              sessionStorage.setItem("email", user.email);
              //console.log(data.val().Role);
              /*if(data.val().Status == "active"){
                  agent = data.val();
              }*/
          });

        //alert(agent.FirstName);
        //return agent;
    });


}


function createAgents(){
    app.ref('Users').push({
        Address: document.getElementById("address").value,
        CompanyID: "some company id",
        ContactNo : document.getElementById("contactNumber").value,
        Email: document.getElementById("email").value,
        FirstName: document.getElementById("FName").value,
        KandyP: "@Test123",
        KandyU: "testuser@dealhunt.aban.com.bz",
        LastName: document.getElementById("LName").value,
        Role:"Agent",
        Status: "active"
    });
}



// Other information
function changeUserStatus(userId, status){
    //alert("Here");
    app.ref("Users/" + userId + "/Status").set(status, function(error) {
        if (error) {
            alert("Data could not be saved." + error);
        } else {
            alert("Data saved successfully.");
        }
    });
}

function getAnActiveAgent(companyId){
    app.ref("Users").orderByChild('CompanyID').equalTo(companyId).once('value').then(function(snapshot) {
        var agent;
          snapshot.forEach(function(data) {
              if(data.val().Status == "active"){
                  agent = data.val();
              }
          });

        alert(agent.FirstName);
        //return agent;
    });
}

function getUserData(){
    app.ref("Users").orderByChild('Email').equalTo(email).once('value').then(function(snapshot) {
        //console.log(JSON.stringify(snapshot.val()));
        //var agent;
          snapshot.forEach(function(data) {
              alert(data);
              //data.val()
              /*if(data.val().Role == "Agent"){
                  sessionStorage.EMAIL = email;
                  sessionStorage.LOGIN = true;
                  window.location="pages/agent.html";
              }else{
                  sessionStorage.EMAIL = email;
                  sessionStorage.LOGIN = true;
                  window.location="index.html";
              }*/
              //console.log(data.val().Role);
              /*if(data.val().Status == "active"){
                  agent = data.val();
              }*/


          });

        //alert(agent.FirstName);
        //return agent;
    });
}
