function InitKandyAnonymous(){

    // Setup Kandy to make and receive calls.
    kandy.setup({
        // Designate HTML elements to be our stream containers.
        remoteVideoContainer: document.getElementById("VIDEO_WINDOW"),
        //localVideoContainer: document.getElementById("local-container"),

        // Register listeners to call events.
        listeners: {
            callinitiated: onCallInitiated,
            callincoming: onCallIncoming,
            callestablished: onCallEstablished,
            callended: onCallEnded
        }
    });

    // Login automatically as the application starts.
    //kandy.login(projectAPIKey, username, password, onLoginSuccess, onLoginFailure);
}

// What to do when a call is initiated.
function onCallInitiated(call, callee) {
    log("Call initiated with " + callee + ". Ringing...");

    // Store the call id, so the caller has access to it.
    callId = call.getId();

    // Handle UI changes. A call is in progress.
    //document.getElementById("make-call").disabled = true;
    //document.getElementById("end-call").disabled = false;
}

// What to do for an incoming call.
function onCallIncoming(call) {
    log("Incoming call from " + call.callerNumber);

    // Store the call id, so the callee has access to it.
    callId = call.getId();

    // Handle UI changes. A call is incoming.
    //document.getElementById("accept-call").disabled = false;
    //document.getElementById("decline-call").disabled = false;
}

// Accept an incoming call.
function acceptCall() {
    // Tell Kandy to answer the call.
    kandy.call.answerCall(callId, showVideo);
    // Second parameter is false because we are only doing voice calls, no video.

    log("Call answered.");
    // Handle UI changes. Call no longer incoming.
    //document.getElementById("accept-call").disabled = true;
    //document.getElementById("decline-call").disabled = true;
}

// Reject an incoming call.
function declineCall() {
    // Tell Kandy to reject the call.
    kandy.call.rejectCall(callId);

    log("Call rejected.");
    // Handle UI changes. Call no longer incoming.
    //document.getElementById("accept-call").disabled = true;
    //document.getElementById("decline-call").disabled = true;
}

// What to do when call is established.
function onCallEstablished(call) {
    log("Call established.");
    $.unblockUI();
    callEstablished();

    // Handle UI changes. Call in progress.
    /*document.getElementById("make-call").disabled = true;
    document.getElementById("mute-call").disabled = false;
    document.getElementById("hold-call").disabled = false;
    document.getElementById("end-call").disabled = false;*/
}

// End a call.
function endCall() {
    // Tell Kandy to end the call.
    kandy.call.endCall(callId);
}

// Variable to keep track of mute status.
var isMuted = false;

// Mute or unmute the call, depending on current status.
function toggleMute() {
    if(isMuted) {
        kandy.call.unMuteCall(callId);
        log("Unmuting call.");
        isMuted = false;
    } else {
        kandy.call.muteCall(callId);
        log("Muting call.");
        isMuted = true;
    }
}

// Variable to keep track of hold status.
var isHeld = false;

// Hold or unhold the call, depending on current status.
function toggleHold() {
    if(isHeld) {
        kandy.call.unHoldCall(callId);
        log("Unholding call.");
        isHeld = false;
    } else {
        kandy.call.holdCall(callId);
        log("Holding call.");
        isHeld = true;
    }
}

// What to do when a call is ended.
function onCallEnded(call) {
    log("Call ended.");

    $('#wasResolvedModal').modal('show');
    //wasResolvedModal

    // Handle UI changes. No current call.
    /*document.getElementById("make-call").disabled = false;
    document.getElementById("mute-call").disabled = true;
    document.getElementById("hold-call").disabled = true;
    document.getElementById("end-call").disabled = true;*/

    // Call no longer active, reset mute and hold statuses.
    isMuted = false;
    isHeld = false;
}



// Show or hide video, depending on current status.
function toggleVideo() {
    if(showVideo) {
        kandy.call.stopCallVideo(callId);
        log("Stopping send of video.");
        showVideo = false;
    } else {
        kandy.call.startCallVideo(callId);
        log("Starting send of video.");
        showVideo = true;
    }
}





var http = createRequestObject();

// Javascript Document
function createRequestObject(){
    var returnObj = false;
    if(window.XMLHttpRequest) {
        returnObj = new XMLHttpRequest();
    } else if(window.ActiveXObject) {
        try {
            returnObj = new ActiveXObject("Msxml2.XMLHTTP");
        } catch (e) {
            try {
                returnObj = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {}
        }
    }
    return returnObj;
}

function getAnonymousUser(){
    InitKandyAnonymous();
    sendRequest('https://api.kandy.io/v1.2/domains/accesstokens?key=DAK2df0886d84f94930a4d2373b906adb21&domain_api_secret=DAS6118ed488bc640978c77c8c161196856', handleDomainAccessResponse);
}

function sendRequest(url, responseObject){
    try{
        http.open('get', url, true);
    }
    catch (e){
        return;
    }

    http.onreadystatechange = responseObject;
    http.send();
}

function handleDomainAccessResponse(){
    //var nod = document.getElementById(target);
    //nod.innerHTML = "<a href='javascript:showModal()' class='closeLink'>[ CLOSE ]</a><p>&nbsp;</p>";
    if(http.readyState == 4) {
        try{
            var strResponse = http.responseText;
            var res = JSON.parse(strResponse);
            //console.log(res.result.domain_access_token);
            if(res.message == "success"){
                var domain_access_token = res.result.domain_access_token;
                console.log("Domain Access Token retrieved...");
                sendRequest('https://api.kandy.io/v1.2/domains/access_token/users/user/anonymous?key=' + domain_access_token, handleAnonymousUserToken);
                //alert(domain_access_token);
            }else{
                console.log("Some shit went wrong");
            }
            //alert(strResponse);

        } catch (e){
           console.log(e);
        }
    }
}

var user;

function handleAnonymousUserToken(){
    if(http.readyState == 4) {
        try{
            var strResponse = http.responseText;
            var res = JSON.parse(strResponse);
            //alert("Anonymouse User: " + strResponse);
            //console.log(strResponse);
            if(res.message == "success"){
                //alert(res.result);
                console.log("Anonymouse User Access Token retrieved...");
                login(res.result);


                //call(res.result)
            }else{
                console.log("Some shit went wrong");
            }
            //alert(strResponse);
            //console.log(obj.message);
        } catch (e){
           console.log(e);
        }
    }
}



function login(anonymousUser){
    //anonymousUserRetreived(res.result);
    // This is your function to retrieve the AUAT from your web server.
    //var auat = getAUATFromServer();

    //var s = JSON.stringify(anonymousUser);
    user = anonymousUser;
    //alert(user_access_token);
    console.log("loggin in...");
    // Login to Kandy
    kandy.loginSSO(anonymousUser.user_access_token, onLoginSuccess, onLoginFailure);
}


function call(busCallID) {
    console.log("calling " + busCallID + "...");
    // Make a video call to support
    kandy.call.makeCall(busCallID, true);
}

function onLoginSuccess() {
    console.log("SuccessLogin.....");
   anonymousUserRetreived(user);
}

function onLoginFailure(){
    console.log("kandy login failed...");
}
