var agentData;

function InitKandy(data){

    /**
     * Kandy.io Call Demo
     * View this tutorial and others at https://developer.kandy.io/tutorials
     */

    agentData = data;

    console.log("Initializing Kandy...");

    // Variables for logging in.
    var projectAPIKey = "DAK2df0886d84f94930a4d2373b906adb21";
    var username = agentData.KandyU;
    var password = agentData.KandyP;

    // Setup Kandy to make and receive calls.
    kandy.setup({
        // Designate HTML elements to be our stream containers.
        remoteVideoContainer: document.getElementById("VIDEO_WINDOW"),
        //localVideoContainer: document.getElementById("LocalVIDEO_WINDOW"),

        // Register listeners to call events.
        listeners: {
            callinitiated: onCallInitiated,
            callincoming: onCallIncoming,
            callestablished: onCallEstablished,
            callended: onCallEnded,

            message: onMessageReceived
        }
    });

    // Login automatically as the application starts.
    kandy.login(projectAPIKey, username, password, onLoginSuccess, onLoginFailure);
}

// What to do on a successful login.
function onLoginSuccess() {
    $.unblockUI();
    log("Login was successful.");
}

// What to do on a failed login.
function onLoginFailure() {
    log("Login failed. Make sure you input the user's credentials!");
}

// Utility function for appending messages to the message div.
function log(message) {
    //document.getElementById("messages").innerHTML += "<div>" + message + "</div>";
    console.log(message);
}

// Variable to keep track of video display status.
var showVideo = true;


// Get user input and make a call to the callee.
function startCall() {
    var callee = document.getElementById("callee").value;

    // Tell Kandy to make a call to callee.
    kandy.call.makeCall(callee, showVideo);

}

// Variable to keep track of the call.
var callId;

// What to do when a call is initiated.
function onCallInitiated(call, callee) {
    log("Call initiated with " + callee + ". Ringing...");

    // Store the call id, so the caller has access to it.
    callId = call.getId();

    // Handle UI changes. A call is in progress.
    //document.getElementById("make-call").disabled = true;
    //document.getElementById("end-call").disabled = false;
}

// What to do for an incoming call.
function onCallIncoming(call) {
    callIncoming(call);
}

function showRinger(call){
    $('#costumModal28').modal('show');

    log("Incoming call from " + call.callerNumber);

    // Store the call id, so the callee has access to it.
    callId = call.getId();

    // Handle UI changes. A call is incoming.
    //document.getElementById("accept-call").disabled = false;
    //document.getElementById("decline-call").disabled = false;
    //acceptCall();
    //$('costumModal28').show();
}

// Accept an incoming call.
function acceptCall() {
    $('#costumModal28').modal('hide');
      timer();
    log(callId+" "+showVideo);
    // Tell Kandy to answer the call.
    kandy.call.answerCall(callId, showVideo);
    // Second parameter is false because we are only doing voice calls, no video.

    log("Call answered.");
    // Handle UI changes. Call no longer incoming.
    //document.getElementById("accept-call").disabled = true;
    //document.getElementById("decline-call").disabled = true;
}

// Reject an incoming call.
function declineCall() {
    // Tell Kandy to reject the call.
    kandy.call.rejectCall(callId);

    log("Call rejected.");
    // Handle UI changes. Call no longer incoming.
    //document.getElementById("accept-call").disabled = true;
    //document.getElementById("decline-call").disabled = true;
}

// What to do when call is established.
function onCallEstablished(call) {
    log("Call established.");
/*
    // Handle UI changes. Call in progress.
    document.getElementById("make-call").disabled = true;
    document.getElementById("mute-call").disabled = false;
    document.getElementById("hold-call").disabled = false;
    document.getElementById("end-call").disabled = false;*/
}

// End a call.
function endCall() {
    // Tell Kandy to end the call.
    kandy.call.endCall(callId);

}

// Variable to keep track of mute status.
var isMuted = false;

// Mute or unmute the call, depending on current status.
function toggleMute() {
    if(isMuted) {
        kandy.call.unMuteCall(callId);
        log("Unmuting call.");
        isMuted = false;
    } else {
        kandy.call.muteCall(callId);
        log("Muting call.");
        isMuted = true;
    }
}

// Variable to keep track of hold status.
var isHeld = false;

// Hold or unhold the call, depending on current status.
function toggleHold() {
    if(isHeld) {
        kandy.call.unHoldCall(callId);
        log("Unholding call.");
        isHeld = false;
    } else {
        kandy.call.holdCall(callId);
        log("Holding call.");
        isHeld = true;
    }
}

// What to do when a call is ended.
function onCallEnded(call) {
    log("Call ended.");
/*
    // Handle UI changes. No current call.
    document.getElementById("make-call").disabled = false;
    document.getElementById("mute-call").disabled = true;
    document.getElementById("hold-call").disabled = true;
    document.getElementById("end-call").disabled = true;*/

    // Call no longer active, reset mute and hold statuses.
    isMuted = false;
    isHeld = false;

    $('#costumModal3').modal('show');
    stoptimer();
    document.getElementById('VIDEO_WINDOW').innerHTML = "";
}

// Show or hide video, depending on current status.
function toggleVideo() {
    if(showVideo) {
        kandy.call.stopCallVideo(callId);
        log("Stopping send of video.");
        showVideo = false;
    } else {
        kandy.call.startCallVideo(callId);
        log("Starting send of video.");
        showVideo = true;
    }
}


//-+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++Messages+++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Gathers the user's input and sends a message to the recipient.
function sendMessage() {
    var recipient = document.getElementById("recipient").value;
    var message = document.getElementById("messageBox").value;

    kandy.messaging.sendIm(recipient, message, onSendSuccess, onSendFailure);
}

// What to do on a successful send message.
function onSendSuccess(message) {
    // Display the message as outgoing.
    //var recipient = message.destination.split("@")[0];
    // Create the message element. Use Lodash to escape the message for security purposes.
    //var element = "<div>Outgoing (" + recipient + "): " + _.escape(message.message.text) + "</div>";
    //document.getElementById("chat-messages").innerHTML += element;
}

// What to do on a failed send message.
function onSendFailure() {
    log("Send Message failed.");
}

/**
 * Called when the `message` event is triggered.
 * Receives the message object as a parameter.
 */
function onMessageReceived(message) {
    // Display the message as incoming.
    var sender = message.sender.user_id;
    // Create the message element. Use Lodash to escape the message for security purposes.
    var element = "<div>Incoming (" + sender + "): " + escape(message.message.text) + "</div>"
    //var element =  _.escape(message.message.text) ;
    //document.getElementById("Sender").innerHTML += sender;
    document.getElementById("received_MSG").innerHTML += element;
}


