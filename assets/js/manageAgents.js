var app;
var agentSection;

function Init_Firebase(){

    $.blockUI({ message: '<h4><img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" />Initializing Components...</h4>' });

    if(sessionStorage.LOGIN){
        sessionStorage.LOGIN = false;

        if (sessionStorage.EMAIL) {
            // Code for localStorage/sessionStorage.
            userEmail = sessionStorage.EMAIL;
            //getUserData();
            //alert(userEmail);
        } else {
            // Sorry! No Web Storage support..
            //alert("Emtpy");
            alert("You Must Login Befor You Can Access this Page");
            window.location("index.html");
        }
    }

    var config = {

        apiKey: "AIzaSyDZ-HANbA7JkIk3irjGKjbimH0p0u-XvdA",
        authDomain: "kalpoint-317f7.firebaseapp.com",
        databaseURL: "https://kalpoint-317f7.firebaseio.com",
        storageBucket: "kalpoint-317f7.appspot.com",
    };

    firebase.initializeApp(config);

    app = firebase.database();
    agentSection = app.ref("Users");
    $.unblockUI();
    retrieveAgentsData();
}

function retrieveAgentsData(){

    $.blockUI({ message: '<h4><img src="http://www.downgraf.com/wp-content/uploads/2014/09/01-progress.gif?673ca9" height="60"  width="80" />Retrieving Agents...</h4>' });

    var container = document.getElementById("listAgents");
    container.setAttribute("style","text-align:center");
    container.innerHTML = "";

    var title = document.createElement("H2");
    title.innerHTML = "Agents In Our System";
    container.appendChild(title);

    var table = document.createElement("TABLE");
    table.className = "table table-bordered";
    container.appendChild(table);

    var TRHead = document.createElement("TR");
    TRHead.setAttribute("style","background-color:#F2F2F2; color:#777;font-size:12px;")
    table.appendChild(TRHead);

    var TDHead = document.createElement("TD");
    TDHead.innerHTML = "Name";

    var TDHead1 = document.createElement("TD");
    TDHead1.innerHTML = "Address";

    var TDHead2 = document.createElement("TD");
    TDHead2.innerHTML = "Email";

    var TDHead3 = document.createElement("TD");
    TDHead3.innerHTML = "Contact Number";

    var TDHead4 = document.createElement("TD");
    TDHead4.innerHTML = "Options";

    TRHead.appendChild(TDHead);
    TRHead.appendChild(TDHead1);
    TRHead.appendChild(TDHead2);
    TRHead.appendChild(TDHead3);
    TRHead.appendChild(TDHead4);

    var body = document.createElement("TBODY");
    body.setAttribute("style","background-color:#FFFFFF")
    table.appendChild(body);

    var agentsRef = agentSection.orderByChild("Role").equalTo("Agent");
    agentSection.on("value", function(snap) {

        snap.forEach(function(data2) {
            //console.log(data2);
            if(data2.val().Role == "Agent"){
            var tempTR = document.createElement("TR");
            tempTR.setAttribute("style","text-align:center");
            body.appendChild(tempTR);

            var tempTD = document.createElement("TD");
            tempTD.innerHTML = data2.val().FirstName +" "+ data2.val().LastName;
            //tempTD.setAttribute("style","")

            var tempTD2 = document.createElement("TD");
            tempTD2.innerHTML = data2.val().Address;

            var tempTD3 = document.createElement("TD");
            tempTD3.innerHTML = data2.val().Email;

            var tempTD4 = document.createElement("TD");
            tempTD4.innerHTML = data2.val().ContactNo;

            var tempTD5 = document.createElement("TD");
            var button = document.createElement("BUTTON");
            button.className = "btn btn-link-1 launch-modal btn-danger";
            button.innerHTML = "Delete Agent";
            button.value = data2.key;
            button.setAttribute("onclick","handleDelete(this)")
            tempTD5.appendChild(button);


            tempTR.appendChild(tempTD);
            tempTR.appendChild(tempTD2);
            tempTR.appendChild(tempTD3);
            tempTR.appendChild(tempTD4);
            tempTR.appendChild(tempTD5);
            }
        });

        $.unblockUI();
            //document.getElementById("totAge").innerHTML=agentCnt;
    });


}

function handleDelete(button){
    alert(button.value);
}
