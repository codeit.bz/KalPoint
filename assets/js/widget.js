var Comp_Name;
var message_reference;
var name_reference;

function init_Widget(NAME_INPUT_REFERENCE, MESSAGE_ID_REFERENCE, companyName){

    console.log(companyName);

    Comp_Name = companyName;
    name_reference = NAME_INPUT_REFERENCE;
    message_reference = MESSAGE_ID_REFERENCE;
}

function WidgetCallback(){

    var encoded_comp_name = "";
    var temp3 = Comp_Name.split(" ");
    for(var i = 0; i < temp3.length; i++){
        encoded_comp_name+=temp3[i];

        if((i+1)<temp3.length)
            encoded_comp_name+="_";
    }

    var rawnameOfClient = document.getElementById(name_reference);
    var nameOfClient = ""
    var temp1 = rawnameOfClient.value.split(" ");
    for(var i = 0; i < temp1.length; i++){
        nameOfClient+=temp1[i];

        if((i+1)<temp1.length)
            nameOfClient+="_";
    }

    var msg_va = document.getElementById(message_reference)
    var temp = msg_va.value.split(" ") ;
    var message_to_agent = "";
    for(var i = 0; i < temp.length; i++){
        message_to_agent+=temp[i];

        if((i+1)<temp.length)
            message_to_agent+="_";
    }

    window.location = "../client.html?company="+encoded_comp_name+"&msg="+message_to_agent+"&name="+nameOfClient;
    //alert("client.html?company="+encoded_comp_name+"&msg="+message_to_agent+"&name="+nameOfClient);
}
